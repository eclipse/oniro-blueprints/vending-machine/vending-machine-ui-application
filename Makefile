#! /usr/bin/make -f
# -*- makefile -*-
# ex: set tabstop=4 noexpandtab:
# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

default: help all
	-@sync

project?=vending-machine-ui-application
url?=https://git.ostc-eu.org/distro/components/vending-machine-ui-application

lvgl=lvgl
lvgl_driver?=wayland
cmake_options?=-DCMAKE_VERBOSE_MAKEFILE:BOOL=ON
cmake_options+=-Dinstall=ON
V?=1

exe?=${project}
sysroot?=${CURDIR}/tmp/sysroot
prefix?=/usr/local
includedir?=${prefix}/include
base_bindir?=/bin
base_libdir?=/lib
bindir?=${prefix}/${base_bindir}
libdir?=${prefix}/${base_libdir}
srcs?=$(wildcard Sources/*.c | sort)
srcs+=$(wildcard Sources/*/*.c | sort)
objs?=${srcs:.c=.o}

# TODO: Pin upstream URL and tags once released
lvgl_branch?=master
lvgl_org?=lvgl
lvgl_url?=https://github.com/${lvgl_org}/lvgl
lvgl_revision?=7b7bed37d3e937c59ec99fccba58774fbf9f1930
lv_drivers_url?=https://github.com/${lvgl_org}/lv_drivers
lv_drivers_branch?=${lvgl_branch}
lv_drivers_revision?=419a757c23aaa67c676fe3a2196d64808fcf2254
lv_lib_png_url?=https://github.com/${lvgl_org}/lv_lib_png
lv_lib_png_branch?=v8.0.2
lv_lib_png_revision?=bf1531afe07c9f861107559e29ab8a2d83e4715a

depsdir?=tmp/deps
sudo?=
export sudo

CFLAGS+=-DLV_CONF_INCLUDE_SIMPLE=1
CFLAGS+=-I${sysroot}${includedir}/
CFLAGS+=-I${sysroot}${includedir}/lvgl
CFLAGS+=-I${sysroot}${includedir}/lvgl/lv_drivers
LDLIBS+=-L${sysroot}/${libdir}
LDLIBS+=-llv_lib_png
LDLIBS+=-llv_drivers
LDLIBS+=-llvgl

ifeq (wayland, ${lvgl_driver})
CFLAGS+=-DUSE_WAYLAND=1
CFLAGS+=$(shell pkg-config --cflags wayland-client 2> /dev/null || echo "")
CFLAGS+=$(shell pkg-config --cflags xkbcommon 2> /dev/null || echo "")
LDLIBS+=-pthread
LDLIBS+=$(shell pkg-config --libs wayland-client 2> /dev/null || echo "-lwayland-client")
LDLIBS+=$(shell pkg-config --libs xkbcommon 2> /dev/null || echo "-lxkbcommon")
endif

CFLAGS+=$(shell pkg-config --cflags json-c 2> /dev/null || echo "")
CFLAGS+=$(shell pkg-config --cflags libwebsockets 2> /dev/null || echo "")
LDLIBS+=$(shell pkg-config --libs json-c 2> /dev/null || echo "-ljson-c")
LDLIBS+=$(shell pkg-config --libs libwebsockets 2> /dev/null || echo "-llibwebsockets")


help:
	@echo "# URL: ${url}"
	@echo "# Usage:"
	@echo "#  make setup # will install build tools"
	@echo "#  make deps # will download build in depsdir and install to sysroot"
	@echo "#  make run # will run demo"
	@echo "# Env:"
	@echo "#  depsdir=${depsdir}"
	@echo "#  sysroot=${sysroot}"
	@echo "#  libdir=${libdir}"
	@echo "#  includedir=${includedir}"

all: help ${exe}
	ls ${exe}

${exe}: ${objs}
	${CC} ${LDFLAGS} -o $@ $^ ${LDLIBS}

exe: ${exe}
	@-sync

clean:
	rm -vf ${exe} *.o */*.o

cleanall: clean
	rm -rf tmp

run: ${exe}
	pidof weston || echo "wayland compositor should be started before please run: weston"
	pidof weston
	{ [ "_" != "_$${XDG_RUNTIME_DIR}" ] || export XDG_RUNTIME_DIR=/run/user/$$(id -u) ; } \
	  && ${<D}/${<F}

install: ${exe}
	${sudo} install -d "${DESTDIR}/${bindir}"
	${sudo} install $< "${DESTDIR}/${bindir}/${project}"


deps/lvgl: ${depsdir}/${lvgl}/lv_conf.h
	cd ${<D} \
	  && export CFLAGS="${CFLAGS}" \
	  && cmake ${cmake_options} . \
	  && make \
	  && make install DESTDIR=${sysroot}

${depsdir}/${lvgl}/lv_conf.h: ${depsdir}/${lvgl}/lv_conf_template.h
	ls $@ \
	|| sed \
	    -e 's|#if 0 .*Set it to "1" to enable .*|#if 1 // Enabled manually|g' \
	    -e 's|\(#define *LV_MEM_CUSTOM *\)0|\1 1|g' \
	< $< > $@

${depsdir}/${lvgl}/%:
	@mkdir -p ${@D}
	git clone --recursive ${lvgl_url} --branch ${lvgl_branch} ${@D}
	cd ${@D} && git reset --hard ${lvgl_revision}

deps/lv_drivers: ${depsdir}/lv_drivers/lv_drv_conf.h
	cd ${<D} \
	  && export CFLAGS="${CFLAGS} -DLV_CONF_INCLUDE_SIMPLE=1\
	    -I${sysroot}/${includedir}/lvgl \
	    -I${sysroot}/${includedir}/ " \
	  && cmake ${cmake_options} . \
	  && make \
	  && make install DESTDIR=${sysroot}

${depsdir}/lv_drivers/lv_drv_conf.h: ${depsdir}/lv_drivers/lv_drv_conf_template.h
	ls $@ \
	|| sed \
	    -e 's|#if 0 .*Set it to "1" to enable the content.*|#if 1 // Enabled manually|g' \
	< $< > $@

${depsdir}/lv_drivers/%:
	@mkdir -p ${@D}
	git clone --recursive ${lv_drivers_url} --branch ${lv_drivers_branch} ${@D}
	cd ${@D} && git reset --hard ${lv_drivers_revision}

deps/lv_lib_png: ${depsdir}/lv_lib_png/lv_png.h
	ls $<
	cd ${<D} \
	  && export CFLAGS="${CFLAGS} -DLV_CONF_INCLUDE_SIMPLE=1\
	    -I${sysroot}/${includedir}/lvgl \
	    -I${sysroot}/${includedir}/ " \
	  && cmake ${cmake_options} . \
	  && make \
	  && make install DESTDIR=${sysroot}

${depsdir}/lv_lib_png/%:
	@mkdir -p ${@D}
	git clone --recursive ${lv_lib_png_url} --branch ${lv_lib_png_branch} ${@D}
	cd ${@D} && git reset --hard ${lv_lib_png_revision}

deps: deps/lvgl deps/lv_drivers deps/lv_lib_png
	@-sync

run/%:
	PATH=${PATH}:. ${@F}
	@echo ""

demo: ${exe}
	@-sync

setup/debian: /etc/debian_version
	-${sudo} apt-get update -y
	${sudo} apt-get install -y \
		gcc \
		git \
		libjson-c-dev \
		libsdl2-dev \
		libwebsockets-dev \
		make \
		pkg-config \
		sudo \
		weston

/etc/debian_version:
	@echo "error: distro not supported please file a bug: ${url}"

setup: setup/debian
	@-sync
