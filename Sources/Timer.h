#pragma once

typedef void (*TimerFuctionPointer)();

int startTimer(int seconds, TimerFuctionPointer function);
void stopTimer();