#include <libwebsockets.h>
#if defined(_MSC_VER) && _MSC_VER
#define HAVE_STRUCT_TIMESPEC
#define PTW32_STATIC_LIB
#undef pid_t
#endif
#include <pthread.h>
#include "ClientConnection.h"
#include "MessageManager.h"

static struct lws_context *context = NULL;
static struct lws *wsClient = NULL;
static int serviceLoopActive = 0;
static int connected = 0;

int clientSend(char *data, size_t lenght)
{
	if(wsClient != NULL && connected == 1)
	{
		char *buffer;

		buffer = malloc(LWS_PRE + lenght);
		memset((void*)&buffer[LWS_PRE], 0, lenght);
		strncpy(&buffer[LWS_PRE], data, lenght);
		lws_write(wsClient, &buffer[LWS_PRE], lenght, LWS_WRITE_TEXT);
		free(buffer);

		return 1;
	}

	return 0;
}

static int clientCallback(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *data, size_t length)
{
    switch(reason)
    {
        case LWS_CALLBACK_CLIENT_RECEIVE:
            parseReceivedPackage((char*)data);
            break;

        case LWS_CALLBACK_CLIENT_ESTABLISHED:
            connected = 1;
            sendSubscription();
            break;

        case LWS_CALLBACK_CLIENT_CONNECTION_ERROR:
            break;

        case LWS_CALLBACK_CLOSED:
            connected = 0;
            break;

        default:
            break;
    }

    return lws_callback_http_dummy(wsi, reason, user, data, length);
}

static const struct lws_protocols protocols[] = {
    { "lws-protocol", clientCallback, 0, 512, },
    { NULL, NULL, 0, 0 }
};

static void* serviceLoop(void *arg)
{
    while(serviceLoopActive)
    {
        lws_service(context, 1000000);
    }

    return NULL;
}

int clientConnect(char *address, int port)
{
    struct lws_context_creation_info contextInfo;
    struct lws_client_connect_info clientInfo;
    pthread_t tid;

    memset(&contextInfo, 0, sizeof(contextInfo));
    memset(&clientInfo, 0, sizeof(clientInfo));

    contextInfo.port = CONTEXT_PORT_NO_LISTEN;
    contextInfo.protocols = protocols;
    contextInfo.gid = -1;
    contextInfo.uid = -1;

    context = lws_create_context(&contextInfo);
    if(!context)
    {
        return 0;
    }


    clientInfo.context = context;
    clientInfo.ssl_connection = 0;
    clientInfo.address = address;
    clientInfo.host = address;
    clientInfo.origin = address;
    clientInfo.port = port;
    clientInfo.path = "/";
    clientInfo.ietf_version_or_minus_one = -1;
    clientInfo.protocol = protocols[0].name;
    clientInfo.pwsi = &wsClient;

    lws_client_connect_via_info(&clientInfo);
    if(wsClient == NULL)
    {
        clientDisconnect();
        return 0;
    }

    serviceLoopActive = 1;
    if(pthread_create(&tid, NULL, &serviceLoop, NULL) != 0)
    {
        clientDisconnect();
        return 0;
    }

    return 1;
}

void clientDisconnect()
{
    if(context != NULL)
    {
        serviceLoopActive = 0;
        lws_context_destroy(context);
        context = NULL;
    }
}
