#include "lvgl/lvgl.h"
#include "VendingMachine.h"
#include "MessageManager.h"
#include "Timer.h"
#include <malloc.h>
#include <stdio.h>

#define PRODUCT_SELECTED_COLOR      0x5d50b4
#define BACKGROUND_COLOR            0x120f23
#define DELIVERED_WAIT_TIMEOUT      20
#define WAITING_DOTS_COLOR          0xB301FB
#define WAITING_DOTS_TIME           1000
#define CONFIRMATION_TIME           3

typedef struct
{
    lv_img_dsc_t *image;
    lv_obj_t *item;
    float price;
    bool selected;
} Product;

typedef struct
{
    lv_obj_t *totalPrice;
    lv_obj_t *lockPanel;
    lv_obj_t *confirmationPanel;
    lv_obj_t *outOfOrderPanel;
    Product *products;
    uint32_t productCount;
    lv_obj_t *dots[3];
} VendingMachine;

static VendingMachine vendingMachine;
static uint32_t LV_UNLOCK_UI_EVENT;
static uint32_t LV_UI_OUTOFORDER_EVENT;
static uint32_t LV_HIDE_CONFIRMATION_EVENT;

LV_IMG_DECLARE(product1)
LV_IMG_DECLARE(product2)
LV_IMG_DECLARE(product3)
LV_IMG_DECLARE(product4)
LV_IMG_DECLARE(product5)
LV_IMG_DECLARE(product6)
LV_IMG_DECLARE(product7)
LV_IMG_DECLARE(product8)
LV_IMG_DECLARE(product9)
LV_IMG_DECLARE(product10)
LV_IMG_DECLARE(vending_machine)

LV_FONT_DECLARE(shadower_28)
LV_FONT_DECLARE(shadower_42)

static void deliveredEventTimeout()
{
    lv_event_send(vendingMachine.outOfOrderPanel, LV_UI_OUTOFORDER_EVENT, NULL);
}

static void updateServerStatus(int deliver)
{
    int *products = malloc(sizeof(int) * vendingMachine.productCount);

    for(uint32_t i = 0; i < vendingMachine.productCount; i++)
    {
        products[i] = (vendingMachine.products[i].selected == true) ? 1 : 0;
    }

    if(deliver)
    {
        sendProductsDelivery(products, vendingMachine.productCount);
        startTimer(DELIVERED_WAIT_TIMEOUT, deliveredEventTimeout);
    }
    else
    {
        sendProductsStatus(products, vendingMachine.productCount);
    }

    free(products);
}

static void updateTotalPrice()
{
    float totalPrice = 0;
    char price[30];

    for(uint32_t i = 0; i < vendingMachine.productCount; i++)
    {
        Product *product = &vendingMachine.products[i];

        if(product->selected == true)
        {
            totalPrice += product->price;
        }
    }

    sprintf(price, "%.2f", totalPrice);
    lv_label_set_text(vendingMachine.totalPrice, price);
}

static int countSelectedProducts()
{
    int count = 0;

    for(uint32_t i = 0; i < vendingMachine.productCount; i++)
    {
        if(vendingMachine.products[i].selected == true) count++;
    }

    return count;
}

static void cleanSelectedProducts()
{
    for(uint32_t i = 0; i < vendingMachine.productCount; i++)
    {
        Product *product = &vendingMachine.products[i];

        lv_obj_set_style_bg_color(product->item, lv_color_hex(BACKGROUND_COLOR), 0);
        product->selected = false;
    }
}

static void waitingDotsAnimation(void *var, int32_t v)
{
    lv_obj_set_style_bg_opa(var, (v <= 50) ? (50 + v) : (150 - v), 0);
}

static void showConfirmation(int show)
{
    if(show)
        lv_obj_clear_flag(vendingMachine.confirmationPanel, LV_OBJ_FLAG_HIDDEN);
    else
        lv_obj_add_flag(vendingMachine.confirmationPanel, LV_OBJ_FLAG_HIDDEN);
}

static void setVendingMachineLocked(int locked)
{
    if(locked)
    {
        for(int i = 0; i < 3; i++)
        {
            lv_anim_t animDot;

            lv_anim_init(&animDot);
            lv_anim_set_var(&animDot, vendingMachine.dots[i]);
            lv_anim_set_values(&animDot, 0, 100);
            lv_anim_set_exec_cb(&animDot, (lv_anim_exec_xcb_t)waitingDotsAnimation);
            lv_anim_set_repeat_count(&animDot, LV_ANIM_REPEAT_INFINITE);
            lv_anim_set_path_cb(&animDot, lv_anim_path_ease_in_out);
            lv_anim_set_delay(&animDot, (WAITING_DOTS_TIME / 2) * i);
            lv_anim_set_repeat_delay(&animDot, WAITING_DOTS_TIME / 2);
            lv_anim_set_time(&animDot, WAITING_DOTS_TIME);
            lv_anim_start(&animDot);
        }
        lv_obj_clear_flag(vendingMachine.lockPanel, LV_OBJ_FLAG_HIDDEN);
    }
    else
    {
        lv_obj_add_flag(vendingMachine.lockPanel, LV_OBJ_FLAG_HIDDEN);
        lv_anim_del_all();
    }
}

static void setVendingMachineOutOfOrder(int outOfOrder)
{
    if(outOfOrder)
        lv_obj_clear_flag(vendingMachine.outOfOrderPanel, LV_OBJ_FLAG_HIDDEN);
    else
        lv_obj_add_flag(vendingMachine.outOfOrderPanel, LV_OBJ_FLAG_HIDDEN);
}

static void productItemClicked(lv_event_t *event)
{
    Product *product = lv_event_get_user_data(event);
    lv_obj_t *item = lv_event_get_target(event);

    product->selected = !product->selected;
    lv_obj_set_style_bg_color(item, lv_color_hex(product->selected ? PRODUCT_SELECTED_COLOR : BACKGROUND_COLOR), 0);

    updateTotalPrice();
    updateServerStatus(0);
}

static void buyButtonClicked(lv_event_t *event)
{
    if(countSelectedProducts() > 0)
    {
        setVendingMachineLocked(1);
        updateServerStatus(1);
        cleanSelectedProducts();
        updateTotalPrice();
    }
}

static void resetButtonClicked(lv_event_t *event)
{
    updateServerStatus(0);
    setVendingMachineOutOfOrder(0);
    setVendingMachineLocked(0);
}

static void hideConfirmationPanelTimeout()
{
    lv_event_send(vendingMachine.confirmationPanel, LV_HIDE_CONFIRMATION_EVENT, NULL);
}

static void hideConfirmationRequest(lv_event_t *event)
{
    showConfirmation(0);
}

static void unlockUIRequest(lv_event_t *event)
{
    updateServerStatus(0);
    setVendingMachineLocked(0);
    stopTimer();

    showConfirmation(1);
    startTimer(CONFIRMATION_TIME, hideConfirmationPanelTimeout);
}

static void outOfOrderUIRequest(lv_event_t *event)
{
    setVendingMachineOutOfOrder(1);
}

static lv_obj_t* createProductsGridItem(Product *product, lv_coord_t width, lv_coord_t height, lv_obj_t *parent)
{
    const uint32_t iconControlWidth = (width * 65) / 100;
    const uint32_t originalIconSize = 256;
    uint32_t iconWidth, iconHeight, zoom, offset;
    lv_obj_t *item, *icon, *label;
    lv_point_t pivot;
    char price[30];

    item = lv_btn_create(parent);
    lv_obj_set_style_radius(item, 5, 0);
    lv_obj_set_style_bg_color(item, lv_color_hex(BACKGROUND_COLOR), 0);
    lv_obj_set_style_shadow_width(item, 0, 0);
    lv_obj_set_style_text_color(item, lv_color_hex(0xFFFFFF), 0);
    lv_obj_set_style_text_font(item, &shadower_28, 0);
    lv_obj_add_event_cb(item, productItemClicked, LV_EVENT_CLICKED, product);
    product->item = item;

    icon = lv_img_create(item);
    lv_img_set_antialias(icon, true);
    lv_img_set_src(icon, product->image);
    lv_img_get_pivot(icon, &pivot); // Using pivot is the only way to know the real image size
    iconWidth = pivot.x * 2;
    iconHeight = pivot.y * 2;
    zoom = (originalIconSize * iconControlWidth) / iconWidth;
    lv_img_set_zoom(icon, zoom);
    lv_obj_set_align(icon, LV_ALIGN_TOP_MID);
    offset = (iconHeight - ((iconHeight * zoom) / originalIconSize)) / 2;
    lv_obj_set_pos(icon, 0, (offset - 10) * -1);

    label = lv_label_create(item);
    sprintf(price, "%.2f", product->price);
    lv_label_set_text(label, price);
    lv_obj_set_align(label, LV_ALIGN_BOTTOM_MID);

    return item;
}

static lv_obj_t* createProductsGrid(uint32_t columnCount, lv_coord_t width, lv_coord_t height, lv_obj_t *parent)
{
    const lv_coord_t cellSpacing = 3;
    static lv_coord_t *rowGrid;
    static lv_coord_t *colGrid;
    lv_coord_t cellWidth, cellHeight;
    lv_obj_t *productsGrid;
    uint32_t rowCount;

    cellWidth = cellHeight = (width - cellSpacing * (columnCount + 1)) / columnCount;
    rowCount = vendingMachine.productCount / columnCount;
    if(vendingMachine.productCount % columnCount) rowCount++;

    rowGrid = malloc(sizeof(lv_coord_t) * (rowCount + 1));
    for(uint32_t i = 0; i < rowCount; i++) rowGrid[i] = cellHeight;
    rowGrid[rowCount] = LV_GRID_TEMPLATE_LAST;
    colGrid = malloc(sizeof(lv_coord_t) * (columnCount + 1));
    for(uint32_t i = 0; i < columnCount; i++) colGrid[i] = cellWidth;
    colGrid[columnCount] = LV_GRID_TEMPLATE_LAST;

    productsGrid = lv_obj_create(parent);
    lv_obj_set_layout(productsGrid, LV_LAYOUT_GRID);
    lv_obj_set_size(productsGrid, width, height);
    lv_obj_set_style_grid_column_dsc_array(productsGrid, colGrid, 0);
    lv_obj_set_style_grid_row_dsc_array(productsGrid, rowGrid, 0);
    lv_obj_set_style_bg_color(productsGrid, lv_color_hex(0xFFFFFF), 0);
    lv_obj_set_style_bg_opa(productsGrid, LV_OPA_100, 0);
    lv_obj_set_style_border_width(productsGrid, 0, 0);
    lv_obj_set_style_radius(productsGrid, 0, 0);
    lv_obj_set_style_pad_top(productsGrid, cellSpacing, 0);
    lv_obj_set_style_pad_bottom(productsGrid, cellSpacing, 0);
    lv_obj_set_style_pad_left(productsGrid, cellSpacing, 0);
    lv_obj_set_style_pad_right(productsGrid, cellSpacing, 0);
    lv_obj_set_style_pad_row(productsGrid, cellSpacing, 0);
    lv_obj_set_style_pad_column(productsGrid, cellSpacing, 0);
    
    for(uint32_t i = 0; i < vendingMachine.productCount; i++)
    {
        const uint8_t col = i % columnCount;
        const uint8_t row = i / columnCount;

        lv_obj_set_grid_cell(createProductsGridItem(&vendingMachine.products[i], cellWidth, cellHeight, productsGrid),
                             LV_GRID_ALIGN_STRETCH, col, 1,
                             LV_GRID_ALIGN_STRETCH, row, 1
        );
    }

    return productsGrid;
}

static lv_obj_t *createCommandBar(lv_coord_t width, lv_coord_t height, lv_obj_t *parent)
{
    lv_obj_t *bar, *label, *priceFrame, *buyButton, *totalPrice, *labelButton;

    bar = lv_obj_create(parent);
    lv_obj_set_size(bar, width, height);
    lv_obj_set_style_bg_color(bar, lv_color_hex(0x34495E), 0);
    lv_obj_set_style_bg_opa(bar, LV_OPA_100, 0);
    lv_obj_set_style_border_width(bar, 0, 0);
    lv_obj_set_style_radius(bar, 0, 0);
    lv_obj_set_style_pad_top(bar, 0, 0);
    lv_obj_set_style_pad_bottom(bar, 0, 0);
    lv_obj_set_style_pad_left(bar, 0, 0);
    lv_obj_set_style_pad_right(bar, 0, 0);

    buyButton = lv_btn_create(bar);
    lv_obj_set_size(buyButton, lv_pct(40), lv_pct(90));
    lv_obj_align(buyButton, LV_ALIGN_RIGHT_MID, -5, 0);
    lv_obj_add_event_cb(buyButton, buyButtonClicked, LV_EVENT_CLICKED, NULL);
    labelButton = lv_label_create(buyButton);
    lv_label_set_text(labelButton, "Buy");
    lv_obj_center(labelButton);
    lv_obj_set_style_shadow_width(buyButton, 0, 0);
    lv_obj_set_style_bg_color(buyButton, lv_color_hex(0x2ECC71), 0);
    lv_obj_set_style_text_font(buyButton, &shadower_42, 0);

    priceFrame = lv_obj_create(bar);
    lv_obj_set_size(priceFrame, lv_pct(30), lv_pct(90));
    lv_obj_align_to(priceFrame, buyButton, LV_ALIGN_OUT_LEFT_MID, -5, 0);
    lv_obj_set_scrollbar_mode(priceFrame, LV_SCROLLBAR_MODE_OFF);
    lv_obj_set_style_bg_color(priceFrame, lv_color_hex(0x000000), 0);
    lv_obj_set_style_bg_opa(priceFrame, LV_OPA_100, 0);
    lv_obj_set_style_border_width(priceFrame, 1, 0);
    lv_obj_set_style_radius(priceFrame, 5, 0);

    label = lv_label_create(bar);
    lv_obj_set_width(label, lv_pct(27));
    lv_label_set_text(label, "Total");
    lv_obj_set_style_text_font(label, &shadower_42, 0);
    lv_obj_set_style_text_color(label, lv_color_hex(0xFFFFFF), 0);
    lv_obj_align_to(label, priceFrame, LV_ALIGN_OUT_LEFT_MID, 0, 0);

    totalPrice = lv_label_create(priceFrame);
    lv_obj_set_align(totalPrice, LV_ALIGN_RIGHT_MID);
    lv_obj_set_style_text_font(totalPrice, &shadower_42, 0);
    lv_obj_set_style_text_color(totalPrice, lv_color_hex(0xFFFFFF), 0);

    vendingMachine.totalPrice = totalPrice;
    updateTotalPrice();
    
    return bar;
}

static lv_obj_t *createAnimatedDot(lv_obj_t *parent)
{
    lv_obj_t *dot;

    dot = lv_obj_create(parent);
    lv_obj_set_scrollbar_mode(dot, LV_SCROLLBAR_MODE_OFF);
    lv_obj_set_style_bg_color(dot, lv_color_hex(WAITING_DOTS_COLOR), 0);
    lv_obj_set_style_radius(dot, LV_RADIUS_CIRCLE, 0);
    lv_obj_set_style_border_width(dot, 0, 0);
    lv_obj_set_size(dot, 30, 30);

    return dot;
}

static lv_obj_t *createLockPanel(lv_obj_t *parent)
{
    lv_obj_t *panel, *icon;

    panel = lv_obj_create(parent);
    lv_obj_set_size(panel, lv_pct(100), lv_pct(100));
    lv_obj_set_align(panel, LV_ALIGN_CENTER);
    lv_obj_add_flag(panel, LV_OBJ_FLAG_HIDDEN);
    lv_obj_add_event_cb(panel, unlockUIRequest, LV_UNLOCK_UI_EVENT, NULL);
    lv_obj_set_style_bg_color(panel, lv_color_hex(BACKGROUND_COLOR), 0);
    lv_obj_set_style_bg_opa(panel, LV_OPA_100, 0);
    lv_obj_set_style_border_width(panel, 0, 0);
    lv_obj_set_style_radius(panel, 0, 0);

    icon = lv_img_create(panel);
    lv_img_set_antialias(icon, true);
    lv_img_set_zoom(icon, 200);
    lv_img_set_src(icon, &vending_machine);
    lv_obj_set_align(icon, LV_ALIGN_CENTER);

    vendingMachine.dots[0] = createAnimatedDot(panel);
    vendingMachine.dots[1] = createAnimatedDot(panel);
    vendingMachine.dots[2] = createAnimatedDot(panel);

    lv_obj_align_to(vendingMachine.dots[1], icon, LV_ALIGN_OUT_BOTTOM_MID, 0, 40);
    lv_obj_align_to(vendingMachine.dots[0], vendingMachine.dots[1], LV_ALIGN_OUT_LEFT_MID, -40, 0);
    lv_obj_align_to(vendingMachine.dots[2], vendingMachine.dots[1], LV_ALIGN_OUT_RIGHT_MID, 40, 0);

    return panel;
}

static lv_obj_t *createConfirmationPanel(lv_obj_t *parent)
{
    lv_obj_t *panel, *icon, *label;

    panel = lv_obj_create(parent);
    lv_obj_set_size(panel, lv_pct(100), lv_pct(100));
    lv_obj_set_align(panel, LV_ALIGN_CENTER);
    lv_obj_add_flag(panel, LV_OBJ_FLAG_HIDDEN);
    lv_obj_add_event_cb(panel, hideConfirmationRequest, LV_HIDE_CONFIRMATION_EVENT, NULL);
    lv_obj_set_style_bg_color(panel, lv_color_hex(BACKGROUND_COLOR), 0);
    lv_obj_set_style_bg_opa(panel, LV_OPA_100, 0);
    lv_obj_set_style_border_width(panel, 0, 0);
    lv_obj_set_style_radius(panel, 0, 0);

    icon = lv_img_create(panel);
    lv_img_set_antialias(icon, true);
    lv_img_set_zoom(icon, 200);
    lv_img_set_src(icon, &vending_machine);
    lv_obj_set_align(icon, LV_ALIGN_CENTER);

    label = lv_label_create(panel);
    lv_obj_set_style_text_color(label, lv_color_hex(0xFFFFFF), 0);
    lv_label_set_text(label, "Products delivered, thank you!");
    lv_obj_set_style_text_font(label, &shadower_42, 0);
    lv_obj_align_to(label, icon, LV_ALIGN_OUT_BOTTOM_MID, 0, 40);

    return panel;
}

static lv_obj_t *createOutOfOrderPanel(lv_obj_t *parent)
{
    lv_obj_t *panel, *label, *resetButton, *labelButton;

    panel = lv_obj_create(parent);
    lv_obj_set_size(panel, lv_pct(100), lv_pct(100));
    lv_obj_set_align(panel, LV_ALIGN_CENTER);
    lv_obj_add_flag(panel, LV_OBJ_FLAG_HIDDEN);
    lv_obj_add_event_cb(panel, outOfOrderUIRequest, LV_UI_OUTOFORDER_EVENT, NULL);
    lv_obj_set_style_bg_color(panel, lv_color_hex(BACKGROUND_COLOR), 0);
    lv_obj_set_style_bg_opa(panel, LV_OPA_100, 0);
    lv_obj_set_style_border_width(panel, 0, 0);
    lv_obj_set_style_radius(panel, 0, 0);

    label = lv_label_create(panel);
    lv_obj_set_style_text_color(label, lv_color_hex(0xFFFFFF), 0);
    lv_label_set_text(label, "Out of order");
    lv_obj_set_style_text_font(label, &shadower_42, 0);
    lv_obj_set_align(label, LV_ALIGN_CENTER);

    resetButton = lv_btn_create(panel);
    lv_obj_set_size(resetButton, lv_pct(40), lv_pct(20));
    lv_obj_align(resetButton, LV_ALIGN_BOTTOM_MID, 0, -20);
    lv_obj_add_event_cb(resetButton, resetButtonClicked, LV_EVENT_CLICKED, NULL);
    labelButton = lv_label_create(resetButton);
    lv_label_set_text(labelButton, "Reset");
    lv_obj_center(labelButton);

    return panel;
}

void runVendingMachine()
{
    const void* icons[10] = { &product1, &product2, &product3, &product4, &product5, &product6, &product7, &product8, &product9, &product10 };
    lv_obj_t *screen = lv_scr_act();
    const lv_coord_t width = lv_obj_get_width(screen);
    const lv_coord_t height = lv_obj_get_height(screen);
    lv_obj_t *productsGrid, *commandBar;
    lv_coord_t productsGridHeight;

    LV_UNLOCK_UI_EVENT = lv_event_register_id();
    LV_UI_OUTOFORDER_EVENT = lv_event_register_id();
    LV_HIDE_CONFIRMATION_EVENT = lv_event_register_id();

    vendingMachine.productCount = 25;
    vendingMachine.products = malloc(sizeof(Product) * vendingMachine.productCount);
    for(uint32_t i = 0; i < vendingMachine.productCount; i++)
    {
        Product *product = &vendingMachine.products[i];

        product->image = icons[i - ((i / 10) * 10)];
        product->price = 1.00f;
        product->selected = false;
    }

    productsGridHeight = (lv_coord_t)(height * 0.85f);
    productsGrid = createProductsGrid(5, width, productsGridHeight, screen);
    lv_obj_set_pos(productsGrid, 0, 0);
    commandBar = createCommandBar(width, height - productsGridHeight, screen);
    lv_obj_set_pos(commandBar, 0, productsGridHeight);
    vendingMachine.lockPanel = createLockPanel(screen);
    vendingMachine.confirmationPanel = createConfirmationPanel(screen);
    vendingMachine.outOfOrderPanel = createOutOfOrderPanel(screen);
}

void productsDelivered()
{
    lv_event_send(vendingMachine.lockPanel, LV_UNLOCK_UI_EVENT, NULL);
}