#include "Timer.h"

TimerFuctionPointer timerFuctionPointer;

#if defined(_MSC_VER) && _MSC_VER
#include <Windows.h>

static HANDLE hTimer = NULL;

static VOID CALLBACK timerCallback(PVOID lpParameter, BOOLEAN TimerOrWaitFired)
{
    timerFuctionPointer();
    stopTimer();
}

int startTimer(int seconds, TimerFuctionPointer function)
{
    timerFuctionPointer = function; 

    if(hTimer != NULL) stopTimer();

    if(CreateTimerQueueTimer(&hTimer, NULL, (WAITORTIMERCALLBACK)timerCallback, NULL, seconds * 1000, 0, WT_EXECUTEINTIMERTHREAD) == 0)
    {
        return 0;
    }

    return 1;
}

void stopTimer()
{
    DeleteTimerQueueTimer(NULL, hTimer, NULL);
    CloseHandle(hTimer);
    hTimer = NULL;
}

#else
#include <sys/time.h>
#include <signal.h>
#include <string.h>

static struct sigaction oldHandler;
static int timerActive = 0;

static void timerHandler(int arg)
{
    timerFuctionPointer();
    stopTimer();
}

int startTimer(int seconds, TimerFuctionPointer function)
{
    struct itimerval timervalue;
    struct sigaction handler;

    timerFuctionPointer = function;

    if(timerActive != 0) stopTimer();

    handler.sa_handler = &timerHandler;
    handler.sa_flags = SA_NODEFER;
    if(sigaction(SIGALRM, &handler, &oldHandler))
    {
        return 0;
    }

    timervalue.it_interval.tv_sec = seconds;
    timervalue.it_interval.tv_usec = 0;
    timervalue.it_value.tv_sec = seconds;
    timervalue.it_value.tv_usec = 0;
    if(setitimer(ITIMER_REAL, &timervalue, NULL))
    {
        sigaction(SIGALRM, &oldHandler, NULL);
        return 0;
    }

    timerActive = 1;

    return 1;
}

void stopTimer()
{
    struct itimerval timervalue;

    memset(&timervalue, 0, sizeof(struct itimerval));
    setitimer(ITIMER_REAL, &timervalue, NULL);
    sigaction(SIGALRM, &oldHandler, NULL);
    timerActive = 0;
}

#endif