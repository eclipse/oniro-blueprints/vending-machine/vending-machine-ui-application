﻿#if defined(_MSC_VER) && _MSC_VER
#include <Windows.h>

#if _MSC_VER >= 1200
// Disable compilation warnings.
#pragma warning(push)
// nonstandard extension used : bit field types other than int
#pragma warning(disable:4214)
// 'conversion' conversion from 'type1' to 'type2', possible loss of data
#pragma warning(disable:4244)
#endif

#include "lvgl/lvgl.h"
#include "lv_lib_png/lv_png.h"
#include "lv_drivers/win32drv/win32drv.h"
#include "VendingMachine.h"
#include "ClientConnection.h"
#include "MessageManager.h"

#define NAME_MAX_LENGTH 500

#if _MSC_VER >= 1200
// Restore compilation warnings.
#pragma warning(pop)
#endif

static void getComputerName(char *name)
{
    TCHAR buffer[NAME_MAX_LENGTH];
    DWORD size = NAME_MAX_LENGTH;
    size_t converterChar;

    GetComputerName(buffer, &size);
    wcstombs_s(&converterChar, name, NAME_MAX_LENGTH, buffer, NAME_MAX_LENGTH);
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                      _In_opt_ HINSTANCE hPrevInstance,
                      _In_ LPWSTR    lpCmdLine,
                      _In_ int       nCmdShow)
{
    char computerName[NAME_MAX_LENGTH];

    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);
    UNREFERENCED_PARAMETER(nCmdShow);
    
    lv_init();
    lv_png_init();

    if(!lv_win32_init(GetModuleHandleW(NULL),
                      SW_SHOW,
                      1024,
                      600,
                      NULL
                      ))
    {
        return -1;
    }

    getComputerName(computerName);
    clientConnect(computerName, 8888);

    runVendingMachine();

    while(!lv_win32_quit_signal)
    {
        lv_timer_handler();
        Sleep(10);
    }

    clientDisconnect();

    return 0;
}
#endif

