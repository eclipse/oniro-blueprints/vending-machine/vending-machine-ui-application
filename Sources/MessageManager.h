#pragma once

void sendSubscription();
void sendProductsStatus(int *products, size_t count);
void sendProductsDelivery(int *products, size_t count);
void parseReceivedPackage(char *data);