#define _DEFAULT_SOURCE /* needed for usleep() */
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>

#if defined(USE_WAYLAND) && USE_WAYLAND
#include <lv_drivers/wayland/wayland.h>
#include <pthread.h>
#else
#define SDL_MAIN_HANDLED /*To fix SDL's "undefined reference to WinMain" issue*/
#include <SDL2/SDL.h>
#endif

#include "lvgl/lvgl.h"
#include "lv_drv_conf.h"
#include "lv_drivers/display/monitor.h"
#include "lv_drivers/display/fbdev.h"
#include "lv_drivers/indev/mouse.h"
#include "lv_drivers/indev/keyboard.h"
#include "lv_drivers/indev/mousewheel.h"
#include "lv_lib_png/lv_png.h"
#include "VendingMachine.h"
#include "ClientConnection.h"
#include "MessageManager.h"

#define NAME_MAX_LENGTH 500

static void hal_init(void);

#if defined(USE_WAYLAND) && USE_WAYLAND
static void * tick_thread(void * data)
{
    (void) data;
    while(true) {
        usleep(5 * 1000);
        lv_tick_inc(5);
    }
}
static void mouse_init() {}
static void keyboard_init() {}
static void mousewheel_init() {}
#else
static int tick_thread(void *data)
{
    (void)data;

    while(1)
    {
        SDL_Delay(5);
        lv_tick_inc(5); /*Tell LittelvGL that 5 milliseconds were elapsed*/
    }

    return 0;
}
#endif


int main(int argc, char **argv)
{
    char hostname[NAME_MAX_LENGTH];
	
    (void)argc;
    (void)argv;

    lv_init();
	lv_png_init();
    hal_init();
	
    gethostname(hostname, NAME_MAX_LENGTH);
    clientConnect(hostname, 8888);

    runVendingMachine();

    while(1)
    {
        lv_timer_handler();
        usleep(5 * 1000);
    }

    return 0;
}

static void hal_init(void)
{
#if defined(USE_WAYLAND) && USE_WAYLAND
    wayland_init();
    static pthread_t hal_thread;
    pthread_create(&hal_thread, NULL, tick_thread, NULL);
#else
    /* Use the 'monitor' driver which creates window on PC's monitor to simulate a display*/
    monitor_init();
    /* Tick init.
    * You have to call 'lv_tick_inc()' in periodically to inform LittelvGL about
    * how much time were elapsed Create an SDL thread to do this*/
    SDL_CreateThread(tick_thread, "tick", NULL);
#endif

    /*Create a display buffer*/
    static lv_disp_draw_buf_t disp_buf1;
#if defined(USE_WAYLAND) && USE_WAYLAND
    static lv_color_t color_buf[WAYLAND_HOR_RES * WAYLAND_VER_RES];
    lv_disp_draw_buf_init(&disp_buf1, color_buf, NULL,
                          WAYLAND_HOR_RES * WAYLAND_VER_RES
			  );
#else   
    static lv_color_t buf1_1[MONITOR_HOR_RES * 100];
    static lv_color_t buf1_2[MONITOR_HOR_RES * 100];
    lv_disp_draw_buf_init(&disp_buf1, buf1_1, buf1_2, MONITOR_HOR_RES * 100);
#endif

    /*Create a display*/
    static lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv); /*Basic initialization*/
    disp_drv.draw_buf = &disp_buf1;
#if defined(USE_WAYLAND) && USE_WAYLAND
    disp_drv.hor_res = WAYLAND_HOR_RES;
    disp_drv.ver_res = WAYLAND_VER_RES;
    disp_drv.flush_cb = wayland_flush;
#else
    disp_drv.flush_cb = monitor_flush;
    disp_drv.hor_res = MONITOR_HOR_RES;
    disp_drv.ver_res = MONITOR_VER_RES;
#endif
    disp_drv.antialiasing = 1;

    lv_disp_t * disp = lv_disp_drv_register(&disp_drv);

    lv_theme_t * th = lv_theme_default_init(disp, lv_palette_main(LV_PALETTE_BLUE), lv_palette_main(LV_PALETTE_RED), LV_THEME_DEFAULT_DARK, LV_FONT_DEFAULT);
    lv_disp_set_theme(disp, th);

    lv_group_t * g = lv_group_create();
    lv_group_set_default(g);

    /* Add the mouse as input device
    * Use the 'mouse' driver which reads the PC's mouse*/
    mouse_init();
    static lv_indev_drv_t indev_drv_1;
    lv_indev_drv_init(&indev_drv_1); /*Basic initialization*/
    indev_drv_1.type = LV_INDEV_TYPE_POINTER;

    /*This function will be called periodically (by the library) to get the mouse position and state*/
#if defined(USE_WAYLAND) && USE_WAYLAND
    indev_drv_1.read_cb = wayland_pointer_read;
#else
    indev_drv_1.read_cb = mouse_read;
#endif
    lv_indev_t *mouse_indev = lv_indev_drv_register(&indev_drv_1);

    keyboard_init();
    static lv_indev_drv_t indev_drv_2;
    lv_indev_drv_init(&indev_drv_2); /*Basic initialization*/
    indev_drv_2.type = LV_INDEV_TYPE_KEYPAD;
#if defined(USE_WAYLAND) && USE_WAYLAND
    indev_drv_2.read_cb = wayland_keyboard_read;
#else
    indev_drv_2.read_cb = keyboard_read;
#endif
    lv_indev_t *kb_indev = lv_indev_drv_register(&indev_drv_2);
    lv_indev_set_group(kb_indev, g);
    mousewheel_init();
    static lv_indev_drv_t indev_drv_3;
    lv_indev_drv_init(&indev_drv_3); /*Basic initialization*/
    indev_drv_3.type = LV_INDEV_TYPE_ENCODER;
#if defined(USE_WAYLAND) && USE_WAYLAND
    indev_drv_3.read_cb = wayland_pointeraxis_read;
#else
    indev_drv_3.read_cb = mousewheel_read;
#endif

    static lv_indev_drv_t touch_drv;
    lv_indev_drv_init(&touch_drv);
    touch_drv.type = LV_INDEV_TYPE_POINTER;
#if defined(USE_WAYLAND) && USE_WAYLAND
    touch_drv.read_cb = wayland_touch_read;
#else
#warning "TODO: Handle touch events without wayland"
#endif
    lv_indev_t * touch_indev = lv_indev_drv_register(&touch_drv);

    lv_indev_t * enc_indev = lv_indev_drv_register(&indev_drv_3);
    lv_indev_set_group(enc_indev, g);
    /*
    LV_IMG_DECLARE(mouse_cursor_icon);
    lv_obj_t * cursor_obj = lv_img_create(lv_scr_act());
    lv_img_set_src(cursor_obj, &mouse_cursor_icon);
    lv_indev_set_cursor(mouse_indev, cursor_obj);
    */
}
