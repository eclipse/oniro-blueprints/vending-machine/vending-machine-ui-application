#include <json-c/json.h>
#include <string.h>
#include <stdio.h>
#include "VendingMachine.h"
#include "ClientConnection.h"
#include "MessageManager.h"

void parseReceivedPackage(char *data)
{
	json_object *jobj = json_tokener_parse(data);

	if(jobj != NULL)
	{
		json_object *messageType;

		json_object_object_get_ex(jobj, "messageType", &messageType);

		if(!strcmp(json_object_get_string(messageType), "event"))
		{
			json_object *messageData, *delivered;

			json_object_object_get_ex(jobj, "data", &messageData);
			if(json_object_object_get_ex(messageData, "delivered", &delivered))
			{
				productsDelivered();
			}
		}
	}
}

static sendPackage(char *message, json_object *data)
{
	json_object *jobj, *messageType;
	char *package;

	jobj = json_object_new_object();
	messageType = json_object_new_string(message);

	json_object_object_add(jobj, "messageType", messageType);
	json_object_object_add(jobj, "data", data);

	package = json_object_to_json_string(jobj);

	clientSend(package, strlen(package));
}

void sendProductsStatus(int *products, size_t count)
{
	json_object *data, *array;

	data = json_object_new_object();
	array = json_object_new_array();

	for(size_t i = 0; i < count; i++)
	{
		json_object_array_add(array, json_object_new_int(products[i]));
	}
	json_object_object_add(data, "selection", array);

	sendPackage("setProperty", data);
}

void sendProductsDelivery(int *products, size_t count)
{
	json_object *data, *array, *deliver, *input;

	data = json_object_new_object();
	deliver = json_object_new_object();
	input = json_object_new_object();
	array = json_object_new_array();

	json_object_object_add(data, "deliver", deliver);
	json_object_object_add(deliver, "input", input);
	for(size_t i = 0; i < count; i++)
	{
		json_object_array_add(array, json_object_new_int(products[i]));
	}
	json_object_object_add(input, "selection", array);

	sendPackage("requestAction", data);
}

void sendSubscription()
{
	json_object *data, *delivered;

	data = json_object_new_object();
	delivered = json_object_new_object();

	json_object_object_add(data, "delivered", delivered);

	sendPackage("addEventSubscription", data);
}
